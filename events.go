package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

var events map[string]string

func reloadEvents() {
	content, err := ioutil.ReadFile("./events.json")
	if err != nil {
		log.Fatal("Error opening events : ", err)
		return
	}

	err = json.Unmarshal(content, &events)

	if err != nil {
		log.Fatal("Error during Unmarshal(): ", err)
	}

}

func sendEvent(name string) {
	reloadEvents()
	text, found := events[name]
	if found {
		appCtx.discordSession.ChannelMessageSend(appCtx.councilRoomId, text)
	}

}
