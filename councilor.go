package main

import "fmt"

func CreateCouncilor(id string) bool {
	_, err := appCtx.dbConnection.Exec("INSERT INTO councilor VALUES (?, -1)", id)
	return err == nil
}

func RemoveCouncilor(id string) bool {
	if Exists(id) {
		_, err := appCtx.dbConnection.Exec("DELETE FROM councilor WHERE id=?", id)
		return err == nil
	}
	return false
}

func SetVote(id string, vote int) {
	appCtx.dbConnection.Exec("UPDATE councilor SET vote=? WHERE id=?", vote, id)
}

func ResetVotes() {
	appCtx.dbConnection.Exec("UPDATE councilor SET vote=-1")
}

func Exists(id string) bool {
	result, err := appCtx.dbConnection.Query("SELECT * FROM councilor WHERE id=?", id)
	if err != nil {
		fmt.Print(err.Error())
		return false
	}
	defer result.Close()
	ok := result.Next()
	return ok
}

func GetVotes() [12]int {
	ret := [...]int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

	result, _ := appCtx.dbConnection.Query("SELECT vote FROM councilor")
	defer result.Close()
	for result.Next() {
		var vote int
		result.Scan(&vote)
		if vote >= 0 && vote < 12 {
			ret[vote]++
		}
	}

	return ret

}
