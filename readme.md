# Shadow council : a role playing bot to manage a coucil of shady people

## Setup

### Database
You will need sqlite3 to operate the bot
run:
> sqlite3 shadowc.db
> .file shadowc.sql

### Configuration

Create a config.json file with

    {
        "bot_token" : "my_token",
        "council_room_id" : "my_id"
    }

the token is your bot token
the id is the id of the room where the bot posts everything in your sever

## Running
just go in the directory and do
    go run *.go

## Todo/Planned features

- allow to configure output to something else than discord for debug/unit tests purposes
- unit tests
- http interface to contol the bot from remote (using echo probably)
- rollable tables