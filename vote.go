package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
)

type vote struct {
	Subject string   `json:"subject"`
	Options []string `json:"options"`
}

var voteQueue []vote

func initVote() {
	voteQueue = make([]vote, 0)
}

func newVote() {

	voteText := fmt.Sprintf("**Un nouveau vote a été soumis au conseil :** \n %v \n", voteQueue[0].Subject)
	voteText += "**les propositions sont les suivantes**"
	for index := range voteQueue[0].Options {
		voteText += fmt.Sprintf("\n%v : %v", index, voteQueue[0].Options[index])
	}
	ResetVotes()
	appCtx.discordSession.ChannelMessageSend(appCtx.councilRoomId, voteText)

}

func queueVoteJson(name string) {
	content, err := ioutil.ReadFile("./vote.json")
	if err != nil {
		log.Fatal("Error opening votes : ", err)
		return
	}
	var voteData map[string]vote
	err = json.Unmarshal(content, &voteData)

	if err != nil {
		log.Fatal("Error during Unmarshal(): ", err)
	}

	toQueue, found := voteData[name]
	if found {
		voteQueue = append(voteQueue, toQueue)
		if len(voteQueue) == 1 {
			newVote()
		}
	}

}

func queueVote(subject string, options []string) bool {
	toAdd := vote{subject, options}
	voteQueue = append(voteQueue, toAdd)
	if len(voteQueue) == 1 {
		newVote()
	}

	return true
}

func endVote() {

	results := GetVotes()
	voteText := "Les ombres ont parlées :"
	for index := range voteQueue[0].Options {
		voteText += fmt.Sprintf("\n - option %v : %v votes", index, results[index])
	}
	appCtx.discordSession.ChannelMessageSend(appCtx.councilRoomId, voteText)

	fmt.Print(voteText)
	if len(voteQueue) == 0 {
		return
	}

	voteQueue = voteQueue[1:]

	if len(voteQueue) == 0 {
		return
	}
	newVote()
}
