package main

import (
	"bufio"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
	_ "github.com/mattn/go-sqlite3"
)

type appConfig struct {
	BotToken      string `json:"bot_token"`
	CouncilRoomId string `json:"council_room_id"`
}

type appContext struct {
	dbConnection   *sql.DB
	discordSession *discordgo.Session
	councilRoomId  string
}

var appCtx *appContext

func readerLoop() {
	for {
		reader := bufio.NewReader(os.Stdin)
		input, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println("erreur de lecture")
		}
		command := strings.Split(input, ";")
		if len(command) == 0 {
			continue
		}
		if command[0] == "end" {
			fmt.Print("end")
			endVote()
		}
		if command[0] == "queue" {
			queueVote(command[1], command[2:])
		}
		if command[0] == "queuej" {
			queueVoteJson(command[1])
		}
		if command[0] == "event" {
			sendEvent(command[1])
		}
		if command[0] == "count" {
			result, err := appCtx.dbConnection.Query("SELECT vote FROM councilor")
			if err != nil {
				fmt.Print("error in sql query", err)
				continue
			}
			count := 0
			vote_count := 0
			for result.Next() {
				var vote int
				result.Scan(&vote)
				if vote != -1 {
					vote_count++
				}
				count++
			}
			fmt.Printf(" %v conseillers dont %v ont voté \n", count, vote_count)

		}
	}
}

func main() {

	appCtx = new(appContext)

	content, err := ioutil.ReadFile("./config.json")
	if err != nil {
		log.Fatal("Error opening events : ", err)
		return
	}
	var config appConfig
	err = json.Unmarshal(content, &config)

	if err != nil {
		log.Fatal("Error during Unmarshal(): ", err)
		return
	}

	//database connection
	appCtx = new(appContext)
	appCtx.councilRoomId = config.CouncilRoomId
	initVote()
	var sqlerr error
	appCtx.dbConnection, sqlerr = sql.Open("sqlite3", "shadowc.db")
	if sqlerr != nil {
		fmt.Println("error opening db connection,", sqlerr)
		return
	}
	defer appCtx.dbConnection.Close()

	dg, err := discordgo.New("Bot " + config.BotToken)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}
	appCtx.discordSession = dg
	// Register the messageCreate func as a callback for MessageCreate events.
	dg.AddHandler(messageCreate)
	// In this example, we only care about receiving message events.
	dg.Identify.Intents = discordgo.IntentDirectMessages
	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}
	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sendEvent("start")
	go readerLoop()
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	sendEvent("end")

	// Cleanly close down the Discord session.
	dg.Close()
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	if m.Author.ID == s.State.User.ID {
		return
	}

	messageChannel, err := s.Channel(m.ChannelID)

	if err != nil {
		return
	}

	if messageChannel.Type != 1 {
		return
	}

	command := strings.Split(m.Content, " ")
	if len(command) < 1 {
		return
	}

	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}
	if m.Content == "join" {
		success := CreateCouncilor(m.Author.ID)
		if success {
			s.ChannelMessageSend(m.ChannelID, "Vous avez rejoint le conseil")
			s.ChannelMessageSend(appCtx.councilRoomId, "Une ombre encapuchonée a rejoint le conseil")
		} else {
			s.ChannelMessageSend(m.ChannelID, "Impossible, êtes vous déja au conseil?")
		}
	}
	if command[0] == "vote" {
		if len(command) != 2 {
			s.ChannelMessageSend(m.ChannelID, "Vous ne pouvez voter que pour un seul choix")
			return
		}
		var voteVal int
		found, readError := fmt.Sscan(command[1], &voteVal)
		if found != 1 {
			s.ChannelMessageSend(m.ChannelID, "Non cela n'est pas la voie")
			fmt.Print(readError.Error())
			return
		}
		SetVote(m.Author.ID, voteVal)
	}
	if command[0] == "wh" {
		if Exists(m.Author.ID) && len(command) > 1 {
			msg := fmt.Sprintf("\"%v\"", strings.Join(command[1:], " "))
			fmt.Printf("%v %v : %v\n", m.Author.ID, m.Author.Username, msg)
			s.ChannelMessageSend(appCtx.councilRoomId, msg)
		}
	}

	if command[0] == "leave" {
		if RemoveCouncilor(m.Author.ID) {
			s.ChannelMessageSend(appCtx.councilRoomId, "Une ombre encapuchonée a quitté le conseil")
		}
	}

	if m.Content == "help" {
		s.ChannelMessageSend(m.ChannelID, "Commandes(minuscules sans espaces) : \njoin : rejoindre le conseil\nleave : quitter le conseil \nvote {numero} : voter pour la décision courante\nwh {message} envoyer anonymement le message (des traces sont gardés par l'entité pour modération)")
	}
}
